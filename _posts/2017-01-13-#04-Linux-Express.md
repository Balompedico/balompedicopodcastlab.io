---
title: "#04 Linux Express"
date: 2017-01-13
author: juan
category: linuxexpress
featimg: 2017/linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2304%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/linuxexpress.png)  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2304%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>
La cuarta entrega de Linux Express, los audios que comparto cada 2 semanas en [Telegram](https://t.me/podcastlinux) para ir alternando
los podcasts quincenales con éstos.
Aquí encontrarás información de lo que estoy proyectando y realizando mientras esperas a un nuevo podcast.
