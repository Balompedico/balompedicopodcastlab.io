# PodcastLinux

Este repositorio es el blog del proyecto Podcast Linux, un podcast del mundo GNU/Linux para los usuarios domésticos de escritorio. 

## Licencias

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
